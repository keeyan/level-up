extends Node

const SAVE_FILE_PATH = "user://savegame.save"
var save_data

func _init():
	if not save_file_exists():
		create_save_file()
	save_data = get_save_contents()


func get_save_contents():
	var save_file = File.new()
	save_file.open(SAVE_FILE_PATH, File.READ)
	var save_file_text = save_file.get_as_text()

	var parsed_json = JSON.parse(save_file_text)

	assert(parsed_json.error == OK)
	
	if parsed_json.error != OK:
		create_save_file()

	return parsed_json.get_result()


func get_score():
	return save_data['score']


func set_score(score):
	save_data['score'] = score
	save()


func create_save_file():
	var save_file = File.new()
	save_file.open(SAVE_FILE_PATH, File.WRITE)
	var default_save_text = '{"score": 0}'
	save_file.store_string(default_save_text)


func save_file_exists():
	return File.new().file_exists(SAVE_FILE_PATH)


func save():
	var file = File.new()
	file.open(SAVE_FILE_PATH, File.WRITE)
	file.store_string(JSON.print(save_data))
	file.close()

