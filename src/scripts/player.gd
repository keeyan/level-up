extends KinematicBody2D

const GRAVITY = 2000.0
const WALK_SPEED = 500
const JUMP_HEIGHT = 800

var velocity = Vector2()
var time_since_last_drop = 1000

onready var collision_shape = get_node("CollisionShape2D")


func _physics_process(delta):
	if is_on_floor():
		if Input.is_action_pressed(("ui_up")):
			velocity.y = -JUMP_HEIGHT
		else:
			velocity.y = 0
	else:
		velocity.y += delta * GRAVITY
		
	if Input.is_action_pressed("ui_left"):
		velocity.x = -WALK_SPEED
	elif Input.is_action_pressed("ui_right"):
		velocity.x =  WALK_SPEED
	else:
		velocity.x = 0

	time_since_last_drop += delta
	if Input.is_action_pressed("ui_down") and time_since_last_drop > 0.5:
		time_since_last_drop = 0
		collision_shape.disabled = true
	if time_since_last_drop > 0.2:
		collision_shape.disabled = false

	return move_and_slide(velocity, Vector2(0, -1))


func _on_PlayerCollisionDetector_body_entered(body):
	if body.is_in_group("platform"):
		GameHandler.set_score(body.level)
