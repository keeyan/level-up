extends KinematicBody2D

export(PackedScene) var player
const GRAVITY = 2000
const WALK_SPEED = 510
const OBSTACLE_SPEED_MULTIPLIER = 0.8

var velocity = Vector2()
var obstacles_colliding_with = 0


func _physics_process(delta):
	if is_on_floor():
		velocity.y = 0
	else:
		velocity.y += delta * GRAVITY

	var speed = WALK_SPEED if player.position.x > position.x else -WALK_SPEED
	
	if abs(player.position.x - position.x) < 50:
		speed = 0
		$Animation.playing = false
	else:
		$Animation.playing = true

	if speed < 0:
		$Animation.flip_h = true
	elif speed > 0:
		$Animation.flip_h = false
	
	if obstacles_colliding_with > 0:
		position.x += speed*delta*OBSTACLE_SPEED_MULTIPLIER
	else:
		velocity.x = speed
		# warning-ignore:return_value_discarded
		move_and_slide(velocity, Vector2(0, -1))


func _on_HitBox_body_entered(body):
	if body.get_name() == "Player":
		GameHandler.end_game()
	if body.is_in_group("obstacles"):
		obstacles_colliding_with += 1


func _on_HitBox_body_exited(body):
	if body.is_in_group("obstacles"):
		obstacles_colliding_with -= 1
