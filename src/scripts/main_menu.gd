extends Control

func _ready():
	set_scoreboard()

func _on_StartGame_pressed():
	start_game()

func start_game():
	var scene_change_status = get_tree().change_scene("res://src/scenes/world.tscn")

	assert(scene_change_status == OK)

func _input(event):
	if event.is_action_pressed("ui_accept"):
		start_game()

func set_scoreboard():
	var score = SaveManager.get_score()
	$Scoreboard.set_text("High Score: " + String(score))
