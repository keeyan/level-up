extends "res://src/scripts/infinite_scene.gd"

const SPEED = 490

func _process(delta):
	._process(delta)
	position.x += SPEED*delta
