extends MarginContainer

func _ready():
	var connect_status = GameHandler.connect("score_changed", self, "_on_score_changed")
	assert(connect_status == OK)

func _on_score_changed(score):
	$Score.bbcode_text = String(score)
