extends Node2D

export var infinite_scene_index = 0

func _enter_tree():
	var x = randf()*1000

	var infinite_scene = get_node("InfinitePlatformInfiniteScene")
	infinite_scene.set_position(Vector2(x,0))
	infinite_scene.level = infinite_scene_index
