extends "res://src/scripts/infinite_scene.gd"

var SPEED = 495

func _physics_process(delta):
	if player.position.x > position.x:
		position.x += delta*SPEED
	else:
		position.x -= delta*SPEED

func create_new_scene():
	var goblin = scene.instance()
	goblin.player = player
	return goblin
