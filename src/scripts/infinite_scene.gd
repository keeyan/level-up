extends Node2D

export(PackedScene) var scene
export var node_distance = 1000
export var place_nodes_vertically = false
export(float) var max_screens_ahead = 1

var scene_segments = Array()

onready var player = get_node("/root/World/Player")

class SceneSegment:
	var scene
	var position
	var index
	func _init(new_scene, scene_position, scene_index):
		scene = new_scene
		position = scene_position
		index = scene_index

func _process(_delta):
	if scene_segments.size() == 0:
		add_first_scene()
	var player_position = get_axis_for_setup(player.position)
	var start_of_first_scene = get_axis_for_setup(beginning_of_first_scene())
	var end_of_last_scene = get_axis_for_setup(end_of_last_scene())
	var screen_size = get_axis_for_setup(screen_size())

	if player_position > end_of_last_scene - screen_size*max_screens_ahead:
		add_scene_to_end()
	if player_position < start_of_first_scene + screen_size*max_screens_ahead:
		add_scene()

	if player_position + screen_size*(max_screens_ahead+1) < end_of_last_scene:
		remove_edge_scene()
	if player_position - screen_size*(max_screens_ahead+1) > start_of_first_scene:
		remove_edge_scene(false)

func remove_edge_scene(from_end = true):
	if scene_segments.size() < 5: return
	
	var scene_to_remove = null
	if from_end:
		scene_to_remove = scene_segments.pop_back()
	else:
		scene_to_remove = scene_segments.pop_front()
	scene_to_remove.scene.queue_free()

func add_scene_to_end():
	var new_index = scene_segments.back().index - 1
	var new_scene = create_new_scene()
	var new_position = add_offset_for_setup(end_of_last_scene())

	if "infinite_scene_index" in new_scene:
		new_scene.infinite_scene_index = new_index


	new_scene.set_position(new_position)
	
	var previous_scene =  scene_segments.back().scene

	if previous_scene.has_method("get_mirror"):
		new_scene.set_mirror(!previous_scene.get_mirror())

	add_scene_to_tree(new_scene)
	var scene_segment = SceneSegment.new(
		new_scene,
		Vector2(new_position),
		new_index
	)
	scene_segments.push_back(scene_segment)


func add_first_scene():
	var new_scene = create_new_scene()

	if "infinite_scene_index" in new_scene:
		new_scene.infinite_scene_index = 0
		
	var scene_segment = SceneSegment.new(new_scene, Vector2(position), 0)
	scene_segments.push_front(scene_segment)
	new_scene.set_position(position)
	add_scene_to_tree(new_scene)


func add_scene():
	var new_index = scene_segments.front().index + 1
	var new_scene = create_new_scene()
	var new_position = subtract_offset_for_setup(beginning_of_first_scene())
	new_scene.set_position(new_position)

	
	if "infinite_scene_index" in new_scene:
		new_scene.infinite_scene_index = new_index

	var previous_scene =  scene_segments.front().scene

	if previous_scene.has_method("get_mirror"):
		new_scene.set_mirror(!previous_scene.get_mirror())

	add_scene_to_tree(new_scene)
	var scene_segment = SceneSegment.new(
		new_scene,
		Vector2(new_position),
		new_index
		)
	scene_segments.push_front(scene_segment)

func end_of_last_scene():
	var scene_position = scene_segments.back().position

	return add_offset_for_setup(scene_position)


func beginning_of_first_scene():
	var scene_position = scene_segments.front().position

	return subtract_offset_for_setup(scene_position)


func get_axis_for_setup(vector):
	if place_nodes_vertically:
		return vector.y
	else:
		return vector.x


func add_offset_for_setup(vector):
	var new_vector = Vector2(vector.x, vector.y)
	
	if place_nodes_vertically:
		new_vector.y += node_distance/2.0
		new_vector.x = position.x
	else:
		new_vector.y = position.y
		new_vector.x += node_distance/2.0
	return new_vector


func subtract_offset_for_setup(vector):
	var new_vector = Vector2(vector.x, vector.y)
	
	if place_nodes_vertically:
		new_vector.y -= node_distance/2.0
		new_vector.x = position.x
	else:
		new_vector.x -= node_distance/2.0
		new_vector.y = position.y
 
	return new_vector


func screen_size():
	return get_viewport().size


func create_new_scene():
	var new_scene = scene.instance()

	return new_scene

func add_scene_to_tree(new_scene):
	get_parent().add_child(new_scene)
