extends StaticBody2D

export var level = 0

var obstacle = preload("res://src/scenes/barbed_wire.tscn")
export var mirror = false

onready var number_of_obstacles_to_spawn = int(rand_range(0, min(5, (level  / 100.0) + 2))) if level > 0 else 0

func _ready():
	for _i in range(0, number_of_obstacles_to_spawn):
		create_obstacle()


func create_obstacle():
	var new_obstacle = obstacle.instance()

	var max_position = get_node("CollisionShape2D").get_shape().get_extents()
	var x = rand_range(0, max_position.x*2)
	var y = -max_position.y
	new_obstacle.set_position(Vector2(x,y))

	add_child(new_obstacle)


func set_mirror(value):
	mirror = value
	get_node("GroundSprite").flip_h = mirror


func get_mirror():
	return mirror
