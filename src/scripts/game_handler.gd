extends Node

var score = 0

signal score_changed

func end_game():
	ScoreManager.new_score(score)
	var change_scene_status = get_tree().change_scene("res://src/scenes/main_menu.tscn")

	assert(change_scene_status == OK)


func set_score(new_score):
	score = new_score

	emit_signal("score_changed", score)
