extends Area2D


func _on_Fire_body_entered(body):
	if body.get_name() == "Player":
		end_game()


func end_game():
	var change_scene_status = get_tree().change_scene("res://src/scenes/main_menu.tscn")
	assert(change_scene_status == OK)
