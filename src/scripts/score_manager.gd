extends Node

class_name ScoreManager

static func new_score(score):
	var old_score = SaveManager.get_score()

	if old_score < score:
		SaveManager.set_score(score)
