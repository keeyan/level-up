extends "res://src/scripts/infinite_scene.gd"

export var infinite_scene_index = -1

func create_new_scene():
	var platform = .create_new_scene()
	platform.level = infinite_scene_index
	return platform
